# DBUS proxy


I needed to proxy the system dbus from a victron venus appliance. This is how I got it to work:



## on the server, AKA the venus appliance

`$ socat TCP-LISTEN:7272,reuseaddr,fork UNIX-CONNECT:/var/run/dbus/system_bus_socket`

## local 

`$ socat TCP:192.168.9.104:7272 ABSTRACT-LISTEN:/tmp/victron_dbus`

## and then monitor that socket locally

`$ sudo dbus-monitor --address unix:abstract=/tmp/victron_dbus`